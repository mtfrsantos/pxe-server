import getopt, sys
import os
import json as j

#! Problema (?) [def json_config_log(self)]: Se o log for executado posteriormente à execução da run() a variavél self.os_selected será igual ao que estiver no config.json mesmo que tenho sido alterado o os;
#! Problema [if __name__ == "__main__"]: O mesmo problema anterior se aplicado ao argumento --os/-o que também não é o mais atual;

# TODO: Tratar melhor o recebimento de sistemas operacionais e sistemas operacionais ativos no pxe menu;

class PXE():

  def __init__(self, interface=None, range_ips=None, local_ip=None, gateway=None, mask=None, server_iso_ip=None, os_name='manual'):
    self.interface = interface
    self.range = range_ips
    self.local_ip = local_ip
    self.mask = mask
    self.gateway = gateway
    self.server_iso_ip = server_iso_ip

    if os_name == 'manual':
      self.os_selected = 0
    elif os_name == 'ubuntu':
      self.os_selected = 1
    elif os_name == 'centos':
      self.os_selected = 2
    elif os_name == 'windows':
      self.os_selected = 3
    else:
      self.os_selected = 0

    self.abspath = os.path.abspath(os.path.dirname(__file__))

  def set_interface(self, interface):
    self.interface = interface

  def set_range_ips(self, range_ips):
    self.range = range_ips

  def set_local_ip(self, local_ip):
    self.local_ip = local_ip

  def set_gateway(self, gateway):
    self.gateway = gateway

  def set_mask(self, mask):
    self.mask = mask

  def set_server_iso_ip(self, server_iso_ip):
    self.server_iso_ip = server_iso_ip

  def select_os(self, os_name):

    if os_name == 'manual':
      self.os_selected = 0
    elif os_name == 'ubuntu':
      self.os_selected = 1
    elif os_name == 'centos':
      self.os_selected = 2
    elif os_name == 'windows':
      self.os_selected = 3
    else:
      self.os_selected = 0

  def command(self, command):
    os.system(command)

  def read_json(self):
    json_dir = os.path.join(self.abspath, 'config.json')
    f = open(json_dir,)
    data = j.load(f)

    self.set_interface(data["interface"])
    self.set_range_ips(data["range_ips"])
    self.set_local_ip(data["local_ip"])
    self.set_gateway(data["gateway"])
    self.set_mask(data["mask"])
    self.set_server_iso_ip(data["server_iso_ip"])

    try:
      self.select_os(data["selected_os"])
    except:
      self.os_selected = 0

  def json_config_log(self):

    cmd = '#################### LOG FILE ####################\n\n->Json config file read:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'interface: {self.interface}\nrange ips: {self.range}\nlocal ip: {self.local_ip}\nserver iso ip: {self.server_iso_ip}\ngateway: {self.gateway}\nmask: {self.mask}\nselected os: {self.os_selected}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def static_ip(self):

    static_ip_dir = '/etc/sysconfig/network-scripts'
    local_interface = 'ifcfg-' + self.interface
    static_ip_dir = os.path.join(static_ip_dir, local_interface)

    file = open(static_ip_dir,'r')
    list_of_lines = file.readlines()

    list_of_lines[3] = 'BOOTPROTO=none\n'

    file = open(static_ip_dir,"w")
    file.writelines(list_of_lines)
    file.close()

    text = f'IPADDR={self.local_ip}\nPREFIX=24\nGATEWAY={self.gateway}\nDNS1={self.gateway}\nDNS2=8.8.8.8\nDNS3=8.8.4.4\nPEERDNS=no'

    file = open(static_ip_dir,'a')
    file.write(text)
    file.close()

    cmd1 = f'ifdown {self.interface}'
    cmd2 = f'ifup {self.interface}'
    cmd3 = f'ip add show {self.interface}'

    self.command(cmd1)
    self.command(cmd2)
    self.command(cmd3)

    print('\n-> Static IP UP!')

  def static_ip_log(self):

    static_ip_dir = f'/etc/sysconfig/network-scripts/ifcfg-{self.interface}'

    cmd = '-> Static IP:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {static_ip_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n\n' >> log.txt")
    cmd = f'ip add show {self.interface} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def install_stuff(self):
    self.command('dnf makecache')
    self.command('dnf install -y dnsmasq syslinux')

    print('\n-> Installation completed!')

  def disable_selinux(self):

    self.command('setenforce 0')
    selinux_dir = '/etc/selinux/config'
    file = open(selinux_dir,'r')
    list_of_lines = file.readlines()
    list_of_lines[6] = 'SELINUX=permissive\n'

    file = open(selinux_dir,'w')
    file.writelines(list_of_lines)
    file.close()

    self.command('sestatus')

    print('\n-> SELinux disabled!')

  def selinux_log(self):

    selinux_dir = '/etc/selinux/config'

    cmd = '-> SELinux:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {selinux_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")
    cmd = 'sestatus >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def disable_firewall(self):

    self.command('systemctl stop firewalld')
    self.command('systemctl disable firewalld')
    self.command('systemctl status firewalld --no-pager')

    print('\n-> Firewall disabled!')

  def firewall_log(self):

    cmd = '-> Firewall:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = 'systemctl status firewalld --no-pager >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def dirmaker(self):

    self.command('mkdir -p /tftp/{boot,grub}')
    # self.command('mkdir -p /var/www/html/{desktop,server}')
    # self.command('mkdir /var/www/html/server/centos8')
    # self.command('mkdir /var/www/html/desktop/focal')
    self.command('mkdir /tftp/boot/{windows10,centos8,focal}')
    # self.command('mkdir /smbshare')
    # self.command('mkdir /nfsshare')
    self.command('mkdir -p /netboot/files')
    self.command('mkdir /tftp/pxelinux.cfg')

    print('\n-> Created directories!')

  def dirmaker_log(self):

    cmd = '-> Directorys:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    boot_dir = '/tftp/boot'
    cmd = f'boot dir: {os.path.isdir(boot_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    grub_dir = '/tftp/grub'
    cmd = f'grub dir: {os.path.isdir(grub_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    local_windows_dir = '/tftp/boot/windows10'
    cmd = f'windows dir: {os.path.isdir(local_windows_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    local_ubuntu_dir = '/tftp/boot/focal'
    cmd = f'ubuntu dir: {os.path.isdir(local_ubuntu_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    local_centos_dir = '/tftp/boot/centos8'
    cmd = f'centos dir: {os.path.isdir(local_centos_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    netboot_dir = '/netboot/files'
    cmd = f'netboot files dir: {os.path.isdir(netboot_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    pxelinux_dir = '/tftp/pxelinux.cfg'
    cmd = f'pxelinux dir: {os.path.isdir(pxelinux_dir)}\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)

    self.command("echo -e '\n' >> log.txt")

  # def nfs_server_config(self):
  #   self.command('systemctl start nfs-server')
  #   self.command('systemctl enable nfs-server')
  #   self.command('systemctl status nfs-server --no-page')
  #   self.command('chown nobody:nobody /nfsshare')
  #   self.command('echo "/nfsshare *(ro)" >> /etc/exports')
  #   self.command('exportfs -r')

  #   print('\n--> Nfs-server configured!')

  # def nfs_log(self):
  #   cmd = '-> NFS:\n\n'
  #   cmd = f"echo -e '{cmd}' >> log.txt"
  #   self.command(cmd)
  #   cmd = 'systemctl status nfs-server --no-page >> log.txt'
  #   self.command(cmd)
  #   self.command("echo -e '\n' >> log.txt")

  # def samba_config(self):

  #   self.command('useradd -s /sbin/nologin win10')
  #   self.command('echo -ne "123\n123\n" | smbpasswd -a -s win10')

  #   text = '\n[install]\n         comment = Installation Media\n         path = /smbshare\n         public = yes\n         writable = yes\n         printable = no\n         browseable = yes\n'

  #   dir_smb_config = '/etc/samba/smb.conf'

  #   file = open(dir_smb_config, 'a')
  #   file.write(text)
  #   file.close()

  #   self.command('systemctl start smb nmb')
  #   self.command('systemctl enable smb nmb')
  #   self.command('systemctl status smb nmb --no-page')

  #   print('\n--> Samba configured!')

  # def samba_log(self):

  #   samba_dir = '/etc/samba/smb.conf'

  #   cmd = '-> Samba:\n\n'
  #   cmd = f"echo -e '{cmd}' >> log.txt"
  #   self.command(cmd)
  #   cmd = f'cat {samba_dir} >> log.txt'
  #   self.command(cmd)
  #   self.command("echo -e '\n' >> log.txt")
  #   cmd = 'systemctl status smb nmb --no-page >> log.txt'
  #   self.command(cmd)
  #   self.command("echo -e '\n' >> log.txt")

  def dnsmasq_config(self):

    ips = self.range.split(',')
    ip_low = ips[0]
    ip_up = ips[1]

    text = f'interface={self.interface}\nbind-interfaces\ndomain=linuxhint.local\ndhcp-range={self.interface},{ip_low},{ip_up},{self.mask},8h\ndhcp-option=option:router,{self.gateway}\ndhcp-option=option:dns-server,{self.gateway}\ndhcp-option=option:dns-server,8.8.8.8\nenable-tftp\ntftp-root=/tftp\ndhcp-boot=pxelinux.0,linuxhint-s80,{self.local_ip}\npxe-prompt="Press F8 for PXE Network boot.",0\npxe-service=x86PC,"Install OS via PXE",pxelinux'

    if not os.path.isfile('/etc/dnsmasq.conf.backup'):
      self.command('mv -v /etc/dnsmasq.conf /etc/dnsmasq.conf.backup')

    dir_dnsmasq_config = '/etc/dnsmasq.conf'

    touch_cmd = f'touch {dir_dnsmasq_config}'

    self.command(touch_cmd)

    file = open(dir_dnsmasq_config,'w')
    file.write(text)
    file.close()

    self.command('systemctl restart dnsmasq')
    self.command('systemctl status dnsmasq --no-pager')
    self.command('systemctl enable dnsmasq')

    print('\n-> Dnsmasq configured!')

  def dnsmasq_log(self):

    dnsmasq_dir = '/etc/dnsmasq.conf'

    cmd = '-> Dnsmasq:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {dnsmasq_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")
    cmd = 'systemctl status dnsmasq --no-pager >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  # def apache_config(self):

  #   self.command('systemctl start httpd')
  #   self.command('systemctl status httpd --no-pager')
  #   self.command('systemctl enable http')

  #   print('\n-> Apache server configured!')

  # def apache_log(self):

  #   cmd = '-> Apache:\n\n'
  #   cmd = f"echo -e '{cmd}' >> log.txt"
  #   self.command(cmd)
  #   cmd = 'systemctl status httpd --no-pager >> log.txt'
  #   self.command(cmd)
  #   self.command("echo -e '\n' >> log.txt")

  def install_centos(self):

    # self.command('cp -v /var/www/html/server/centos8/images/pxeboot/{initrd.img,vmlinuz} /tftp/boot/centos8/')
    cmd1 = f'curl http://{self.server_iso_ip}/server/centos8/images/pxeboot/initrd.img --output /tftp/boot/centos8/initrd.img'
    cmd2 = f'curl http://{self.server_iso_ip}/server/centos8/images/pxeboot/vmlinuz --output /tftp/boot/centos8/vmlinuz'

    self.command(cmd1)
    self.command(cmd2)

    print('\n-> CentOS installed!')

  def install_windows(self):

    # self.command('gdown --id 1a1T7bSLXnB-yL1y7VvCzoPSANZKvW0lD -O /tftp/boot/windows10/winpe.iso')
    cmd = f'curl http://{self.server_iso_ip}/initrd/windows10/winpe.iso --output /tftp/boot/windows10/winpe.iso'

    self.command(cmd)

    print('\n-> Windows installed!')

  def install_ubuntu(self):

    self.command('cd /netboot')
    self.command('wget http://archive.ubuntu.com/ubuntu/dists/focal/main/installer-amd64/current/legacy-images/netboot/netboot.tar.gz')
    self.command('tar xvf netboot.tar.gz -C /netboot/files')
    self.command('rm -r netboot.tar.gz')
    self.command('cd')
    self.command('cp -v /netboot/files/ubuntu-installer/amd64/{linux,initrd.gz} /tftp/boot/focal/')

    print('\n-> Ubuntu installed!')

  def syslinux_config(self):

    self.command('cp -v /usr/share/syslinux/{pxelinux.0,memdisk,menu.c32,ldlinux.c32,libutil.c32,vesamenu.c32,lpxelinux.0,libcom32.c32} /tftp/')

    print('\n-> Syslinux configured!')

  def install_menu(self):

    default_menu_dir = '/tftp/pxelinux.cfg/default'

    if not os.path.isfile(default_menu_dir):
      touch_default_menu = f'touch {default_menu_dir}'
      self.command(touch_default_menu)

    else:
      cmd = f'rm -f {default_menu_dir}'
      self.command(cmd)
      touch_default_menu = f'touch {default_menu_dir}'
      self.command(touch_default_menu)

    if self.os_selected == 0:
      text = f'DEFAULT vesamenu.c32\nMENU TITLE ULTIMATE PXE SERVER - By Griffon - Ver 2.0\nPROMPT 0\nTIMEOUT 0\n\nMENU COLOR TABMSG  37;40  #ffffffff #00000000\nMENU COLOR TITLE   37;40  #ffffffff #00000000\nMENU COLOR SEL      7     #ffffffff #00000000\nMENU COLOR UNSEL    37;40 #ffffffff #00000000\nMENU COLOR BORDER   37;40 #ffffffff #00000000\n\nLABEL Ubuntu 20\n    kernel /boot/focal/linux\n    initrd /boot/focal/initrd.gz\n    append ip=dhcp vga=normal mirror/country=br mirror/http/hostname={self.server_iso_ip} mirror/http/directory=/desktop/focal ks=http://{self.server_iso_ip}/desktop/focal/preseed/ubuntu.seed --- quiet\n\nLABEL CentOS 8\n    kernel /boot/centos8/vmlinuz\n    initrd /boot/centos8/initrd.img\n    append ip=dhcp inst.repo=http://{self.server_iso_ip}/server/centos8/\n\nLABEL Windows 10\n    kernel memdisk\n    initrd /tftp/boot/windows10/winpe.iso\n    append iso raw\n'
    else:
      text = f'DEFAULT {self.os_selected}\n\nLABEL 1\n    menu label ^Ubuntu 20\n    kernel /boot/focal/linux\n    initrd /boot/focal/initrd.gz\n    append ip=dhcp vga=normal mirror/country=br mirror/http/hostname={self.server_iso_ip} mirror/http/directory=/desktop/focal ks=http://{self.server_iso_ip}/desktop/focal/preseed/ubuntu.seed --- quiet\n\nLABEL 2\n    menu label ^CentOS 8\n    kernel /boot/centos8/vmlinuz\n    initrd /boot/centos8/initrd.img\n    append ip=dhcp inst.repo=http://{self.server_iso_ip}/server/centos8/\n\nLABEL 3\n    menu label ^Windows 10\n    kernel memdisk\n    initrd /tftp/boot/windows10/winpe.iso\n    append iso raw\n'

    file = open(default_menu_dir,'w')
    file.write(text)
    file.close()

    print('\n-> Menu configured!')

  def menu_log(self):

    default_menu_dir = '/tftp/pxelinux.cfg/default'

    cmd = '-> Menu:\n\n'
    cmd = f"echo -e '{cmd}' >> log.txt"
    self.command(cmd)
    cmd = f'cat {default_menu_dir} >> log.txt'
    self.command(cmd)
    self.command("echo -e '\n' >> log.txt")

  def status(self):

    self.command('systemctl status dnsmasq --no-pager')

  def start(self):

    self.command('systemctl start dnsmasq')

  def run(self, log=True):

    self.static_ip()
    self.install_stuff()
    self.disable_selinux()
    self.disable_firewall()
    self.dirmaker()
    # self.nfs_server_config()
    # self.samba_config()
    self.dnsmasq_config()
    # self.apache_config()
    self.syslinux_config()
    self.install_menu()

    self.install_centos()
    self.install_windows()
    self.install_ubuntu()

    if log:
      self.logs()


  def logs(self):

    self.json_config_log()
    self.static_ip_log()
    self.selinux_log()
    self.firewall_log()
    self.dirmaker_log()
    # self.nfs_log()
    # self.samba_log()
    self.dnsmasq_log()
    # self.apache_log()
    self.menu_log()

if __name__ == "__main__":

  pxe = PXE()
  pxe.read_json()

  # Get full command-line arguments
  full_cmd_arguments = sys.argv

  # Keep all but the first
  argument_list = full_cmd_arguments[1:]

  short_options = "r:nls:o"
  long_options = ["run=", "nolog", "list","select=","os"]

  # print(argument_list)

  if len(argument_list)==0:
    pxe.run()
  else:
    try:
      arguments, values = getopt.getopt(argument_list, short_options, long_options)
    except getopt.error as err:
      # Output error, and return with an error code
      print(str(err))
      sys.exit(2)

    run_value = ''
    nolog_value = False
    list_value = False

    # Evaluate given options
    for current_argument, current_value in arguments:
      if current_argument in ("-r", "--run"):
        run_value = current_value
      elif current_argument in ("-n", "--nolog"):
        nolog_value = True
      elif current_argument in ("-l", "--list"):
        list_value = True
      elif current_argument in ("-s", "--select"):
        if current_value == 'manual' or current_value == 'ubuntu' or current_value == 'centos' or current_value == 'windows':
          pxe.select_os(current_value)
          pxe.install_menu()
        else:
          print(current_value)
          print('OS not found!')
      elif current_argument in ("-o", "--os"):
        print(self.os_selected)

    if not(run_value == ''):
      getattr(pxe, run_value)()

    if nolog_value:
      pxe.run(False)

    if list_value:
      print(str(dir(pxe)))

